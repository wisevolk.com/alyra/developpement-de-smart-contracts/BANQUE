pragma solidity ^0.6.0;

contract Banque {

    string public name;
    address private director;
    uint fondsPropres;
    Client[] clients;
    Conseiller[] conseillers;

    struct Client {
        string nom;
        uint256 solde;
        bytes32 numCompte;
    }

    struct Conseiller {
        string nom;
    }


    constructor(string memory _name) public {
        director = msg.sender;
        name = _name;
        fondsPropres = 1000000000 ether;
    }

    function ajouterUnConseiller(string memory _nom) public {
        require(msg.sender == director);
        conseillers.push(Conseiller(_nom));
    }

    function virement(string memory _envoyeur, string memory _beneficiaire, uint256 _montant) public {
    }

    function ouvertureCompte(string memory _nom, uint256 _depot) public {
        Client memory client = Client(_nom, _depot, keccak256(abi.encode(_nom, _depot)));
        clients.push(client);
    }

    function braquage(string memory _braqueur) public {

    }
}
